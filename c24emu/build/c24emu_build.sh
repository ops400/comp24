IMGUI_OS_DIR="imgui_os"
RLIMGUI_OS_DIR="rlImGui_os"
IGFD_OS_DIR="igfd_os"
C24EMU_OS_DIR="c24emu_os"

if [[ `ls -1 ./$IMGUI_OS_DIR/*.o 2>/dev/null | wc -l ` -gt 4 ]]
then
    echo "imgui found"
else
    echo "imgui not found!"
    exit 1
fi

if [[ `ls -1 ./$RLIMGUI_OS_DIR/*.o 2>/dev/null | wc -l ` -gt 0 ]]
then
    echo "rlImGui found"
else
    echo "rlImGui not found!"
    exit 1
fi

if [[ `ls -1 ./$IGFD_OS_DIR/*.o 2>/dev/null | wc -l ` -gt 0 ]]
then
    echo "ImGui File Dialog found"
else
    echo "ImGui File Dialog not found!"
    exit 1
fi

if [[ -d $C24EMU_OS_DIR ]]
then
    echo "c24emu .os directory found"
    cd $C24EMU_OS_DIR
    g++ -c ../../src/*.cpp
    cd ..
else
    echo "c24emu .os directory not found!"
    exit 1
fi

g++ -o c24emu ./$C24EMU_OS_DIR/*.o ./$IMGUI_OS_DIR/*.o ./$RLIMGUI_OS_DIR/*.o ./$IGFD_OS_DIR/*.o -lraylib

# g++ ./src/*.cpp -o c24emu -lraylib
# g++ ./src/*.cpp -o c24emu
