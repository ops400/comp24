read -p "Are you sure? Yy = yes " -n 1 -r
echo; echo    # new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    if [ -d "imgui_os" ]
    then
        cd imgui_os
        g++ -c ../../src/deps/imgui/*.cpp
        cd ..
        echo "Dear ImGui done!"
    else
        echo "The directory imgui_os does not exist"
        echo "Please create it"
    fi

    if [ -d "rlImGui_os" ]
    then
        cd rlImGui_os
        g++ -c ../../src/deps/rlImGui/*.cpp
        cd ..
        echo "rlImGui done!"
    else
        echo "The directory rlImGui_os does not exist"
        echo "Please create it"
    fi

    if [ -d "igfd_os" ]
    then
        cd igfd_os
        g++ -c ../../src/deps/imGuiFileDialog/*.cpp
        cd ..
        echo "ImGui File Dialog done!"
    else
        echo "The directory igfd_os does not exist"
        echo "Please create it"
    fi
fi


# thx to
# https://stackoverflow.com/questions/1885525/how-do-i-prompt-a-user-for-confirmation-in-bash-script
# https://stackoverflow.com/questions/59838/how-do-i-check-if-a-directory-exists-or-not-in-a-bash-shell-script
# https://linuxize.com/post/bash-if-else-statement/
# https://stackoverflow.com/questions/14765569/how-to-test-if-multiple-files-exist-using-a-bash-script
