#ifndef GUI_HPP_
#define GUI_HPP_
#include "deps/imgui/imgui.h"
#include <raylib.h>

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480
#define WINDOW_TITLE "C24Emu"
const unsigned int WINDOWS_FLAGS = FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE;
const ImGuiWindowFlags mainWindowFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar;


#endif
