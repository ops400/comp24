#include "superVerbose.hpp"
#include "emulatorCore.hpp"
#include "fileManager.hpp"
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <string>
#include <vector>

const std::string stringRegsiters[6] = {"A", "W", "X", "Y", "Z", "PIOR"};
const std::string stringCpuStates[4] = {"Z", "N", "H", "C"};
const std::string stringInstruction[29]{
    "nop",
    "ldm",
    "ldr",
    "str",
    "stv",
    "adm",
    "adr",
    "adv",
    "sbm",
    "sbr",
    "sbv",
    "jpm",
    "jpz",
    "jpc",
    "jpn",
    "jph",
    "cmp",
    "brk",
    "lsf",
    "rsf",
    "inr",
    "inc",
    "dec",
    "mum",
    "mur",
    "muv",
    "dim",
    "dir",
    "divC24" // naming change because of stupid C naming (stdlib.h's div)
};

void superVerbose(std::string filePath, std::string memoryDumpFilePath, bool dumpMemory){
    FileManagerEx fileManager;
    std::vector<uint8_t> initialRomVec = fileManager.openROM(filePath);
    uint8_t initialRom[initialRomVec.size()];
    switch(initialRomVec.size()){
        case 0:
            printf("The rom that you have loaded, seems to empty\nInitial rom size: %lu\n", initialRomVec.size());
            break;
        default:
        std::copy(initialRomVec.begin(), initialRomVec.end(), initialRom);
    }

    size_t initialRomSize = sizeof(initialRom)/sizeof(initialRom[0]);

    // TEMP DEBUG
    printf("initialRomSize: %lu\n", initialRomSize);
    // TEMP DEBUG

    comp24 cpu(initialRom, initialRomSize);

    do{
        cpu.instructionHandler();
        for(uint8_t i = 0; i < 6; i++){
            printf("%s: %hu\n", stringRegsiters[i].c_str(), cpu.getRegister(static_cast<registersEnum>(i)));
        }
        printf("\n");

        printf("PC: %u\n\n", cpu.getPc());
        printf("MIO: %u\n\n", cpu.getMio());
        printf("Instruction: %s\n\n", stringInstruction[static_cast<int>(cpu.getCurrentInstruction())].c_str());
        instructionVector currentInstructionVector = cpu.getCurrentInstuctionVector();
        printf("Current instruction vector:");
        for(uint8_t i = 0; i < 5; i++){
            printf("%X ", currentInstructionVector.bytes[i]);
        }
        printf("\n");
    }while(cpu.getPc()+1 < initialRomSize && !cpu.getGotToBreak());

    if(dumpMemory){
        std::vector<uint8_t> wholeMemory(cpu.memory, cpu.memory + COMP24_MEM_MAX);
        fileManager.saveMemDump(filePath, wholeMemory);
        wholeMemory.clear();
    }

    return;
}
