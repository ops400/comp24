#include "fileManager.hpp"
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <istream>
#include <iterator>
#include <string>
#include <sys/types.h>
#include <vector>

std::vector<uint8_t> FileManagerEx::openROM(std::string filePath){
    // TEMP DEBUG
    printf("%s\n", filePath.c_str());
    // TEMP DEBUG

    std::ifstream input(filePath, std::ios::binary);
    if(!input)
        return std::vector<uint8_t>();

    std::vector<uint8_t> returnVec(std::istreambuf_iterator<char>(input), {});
    // thx https://stackoverflow.com/questions/5420317/reading-and-writing-binary-file

    return returnVec;
}

int FileManagerEx::saveMemDump(std::string filePath, std::vector<uint8_t> wholeMemory){
    std::ofstream output(filePath, std::ios::out | std::ios::binary);
    if(!output){
        printf("Error at file saving\n");
        return 1;
    }

    for(uint32_t i = 0; i < wholeMemory.size(); i++){
        output.write((char*)&wholeMemory[i], sizeof(uint8_t));
    }

    output.close();

    if(!output.good()){
        printf("Error at file writing\n");
        return 2;
    }

    return 0;
}
