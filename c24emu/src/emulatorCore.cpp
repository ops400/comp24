#include "emulatorCore.hpp"
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>

uint32_t brknDwn24BitToUint32(bit24 value){
    uint32_t final = 0;
    for(int8_t i = 2; i >= 0; i--){
        final = final << 8;
        final += value.bytes[i];
    }
    // printf("24b: %.X\n", final);
    return final;
}

uint16_t brknDwn16BitToUint16(bit16 value){
    uint16_t final = 0;
    for(int8_t i = 1; i > -1; i--){
        final = final << 8;
        final += value.bytes[i];
    }
    // printf("16b: %.X\n", final);
    return final;
}

comp24::comp24(uint8_t* initalRom, uint32_t initalRomLength){
    // unsigned long int initalRomLength = (int)sizeof(initalRom)/sizeof(*initalRom);
    memory = (uint8_t*)malloc(COMP24_MEM_MAX *  sizeof(uint8_t));
    for(uint32_t i = 0; i < COMP24_MEM_MAX; i++) memory[i] = 0;
    for(uint32_t i = 0; i < initalRomLength; i++){
        memory[i] = initalRom[i];
    }
    pc = 5;
    currentInstruction = (instructions)memory[5];
    for(uint8_t i = 0; i < 6; i++) registers[i] = 0;
    cpuStates[0] = true;
    for(uint8_t i = 1; i < 4; i++) cpuStates[i] = false;
    for(uint8_t i = 0; i < 5; i++){
        instructionVectorInner.bytes[i] = memory[i+5];
    }

    bit24 tempTransferValue;
    for(int8_t i = 2; i > -1; i--){
        tempTransferValue.bytes[i] = memory[i];
        printf("%d: %X\n", i, memory[i]);
    }
    jumpPinAddress = brknDwn24BitToUint32(tempTransferValue);

    bit16 tempTransferValue16;
    for(uint8_t i = 0; i < 2; i++){
        tempTransferValue16.bytes[i] = memory[i+3];
    }
    pioConfig = brknDwn16BitToUint16(tempTransferValue16);

    // 0? would not be i?
    for(uint32_t i = 0; i < 2; i++){
        // I hope that I did not break anything
        tempTransferValue16.bytes[i] = memory[(uint32_t)(i+COMP24_MEM_MAX-2)];
    }
    pioSates = brknDwn16BitToUint16(tempTransferValue16);

    mio = memory[pc];
    memoryAccessPin = false;
    return;
}

comp24::~comp24(void){
    free(memory);
}

uint16_t comp24::getRegister(registersEnum registerToGet){
    return registers[(int)registerToGet];
}

bool comp24::getCpuState(cpuStatesEnum cpuStateToGet){
    return cpuStates[(int)cpuStateToGet];
}

uint32_t comp24::getPc(void){
    return pc;
}

instructionVector comp24::getCurrentInstuctionVector(void){
    return instructionVectorInner;
}

uint32_t comp24::getJumpPinAddress(void){
    return jumpPinAddress;
}

uint16_t comp24::getPioConfig(void){
    return pioConfig;
}

uint16_t comp24::getPioState(void){
    return pioSates;
}

uint8_t comp24::getMio(void){
    return mio;
}

bool comp24::getMemoryAccessPin(void){
    return memoryAccessPin;
}

instructions comp24::getCurrentInstruction(void){
    return currentInstruction;
}

void comp24::setRegister(registersEnum registerToSet, uint16_t value){
    registers[(int)registerToSet] = value;
    return;
}
void comp24::setCpuState(cpuStatesEnum cpuStateToSet, bool value){
    cpuStates[(int)cpuStateToSet] = value;
    return;
}
bool comp24::setPc(uint32_t address){
    if(address > COMP24_BIT_MAX){
        return false;
    }
    else{
        pc = address;
        return true;
    }
    return false;
}
void comp24::setInstructionVector(instructionVector instructionVectorInput){
    instructionVectorInner = instructionVectorInput;
    return;
}
bool comp24::setJumpPinAddress(uint32_t address){
    if(address > COMP24_BIT_MAX){
        return false;
    }
    else{
        jumpPinAddress = address;
        return true;
    }
    return false;
}
void comp24::setPioConfig(uint16_t value){
    pioConfig = value;
    return;
}
void comp24::setPioStates(uint16_t value){
    pioSates = value;
    return;
}
//   1 2 3 4 5
// A X X X X X
// F Y Y Y Y Y
// 0 1 2 3 4 5 6 7 8 9 A B C D E F 10 11 12 13
// W W W W W X X X X X Y Y Y Y Y Z Z  Z  Z  Z
void comp24::advancePc(){
    pc += 5;
    return;
}

void comp24::instructionHandler(){
    if(!gotToBreak){
        instructionVectorGetFromMemory();
        setCurrentInstruction();
        switch(currentInstruction){

            case nop:
                break;
             case ldm:
                ldmFunction();
            break;
            case ldr:
                ldrFunction();
                break;
            case str:
                strFunction();
                break;
            case stv:
                stvFunction();
                break;
            case adm:
                admFunction();
                break;
            case adr:
                adrFunction();
                break;
            case adv:
                advFunction();
                break;
            case sbm:
                sbmFunction();
                break;
            case sbr:
                sbrFunction();
                break;
            case sbv:
                sbvFunction();
                break;
            case jpm:
                jpmFunction();
                break;
            case jpz:
                jpzFunction();
                break;
            case jpc:
                jpcFunction();
                break;
            case jpn:
                jpnFunction();
                break;
            case jph:
                jphFunction();
                break;
            case cmp:
                cmpFunction();
                break;
            case brk:
                brkFunction();
                break;
            case lsf:
                lsfFunction();
                break;
            case rsf:
                rsfFunction();
                break;
            case inr:
                inrFunction();
                break;
            case inc:
                incFunction();
                break;
            case dec:
                decFunction();
                break;
            case mum:
                mumFunction();
                break;
            case mur:
                murFunction();
                break;
            case muv:
                muvFunction();
                break;
            case dim:
                dimFunction();
                break;
            case dir:
                dirFunction();
                break;
            case divC24:
                divFunction();
                break;
        }
        //...
        advancePc();
    }
    return;
}

void comp24::instructionVectorGetFromMemory(void){
    for(uint32_t i = pc; i < pc+5; i++){
        instructionVectorInner.bytes[i-pc] = memory[i];
    }
    return;
}

void comp24::setCurrentInstruction(void){
    currentInstruction = (instructions)instructionVectorInner.bytes[0];
    return;
}

uint32_t comp24::cpyAddress(){
    uint32_t final = 0;
    bit24 conv;
    for(uint8_t i = 0; i < 3; i++){
        conv.bytes[i] = instructionVectorInner.bytes[i+2];
    }
    final = brknDwn24BitToUint32(conv);
    return final;
}

uint16_t comp24::cpy16bitValueInstructionVector(){
    uint16_t final = 0;
    bit16 conv;
    for(uint8_t i = 0; i < 2; i++){
        conv.bytes[i] = instructionVectorInner.bytes[i+2];
    }
    final = brknDwn16BitToUint16(conv);
    return final;
}

uint16_t comp24::cpy16bitValueMemory(uint32_t address){
    uint16_t final = 0;
    bit16 conv;
    // printf("address: %X\n", address);
    for(uint8_t i = 0; i < 2; i++){
        conv.bytes[i] = memory[i+address];
        // printf("conv: %X\n", conv.bytes[i]);
    }
    final = brknDwn16BitToUint16(conv);
    return final;
}

void comp24::ldmFunction(void){
    uint32_t address = cpyAddress();
    registers[(int)instructionVectorInner.bytes[1]] = memory[address];
    return;
}

void comp24::ldrFunction(void){
    registers[(int)instructionVectorInner.bytes[2]] = registers[(int)instructionVectorInner.bytes[1]];
    return;
}
void comp24::strFunction(void){
    uint32_t address = cpyAddress();
    memory[address] = registers[(int)instructionVectorInner.bytes[1]];
    return;
}

void comp24::stvFunction(void){
    uint32_t address = cpyAddress();
    memory[address] = instructionVectorInner.bytes[1];
    return;
}

void comp24::admFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] + cpy16bitValueMemory(cpyAddress());
    return;
}

void comp24::adrFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] + registers[instructionVectorInner.bytes[2]];
    return;
}

void comp24::advFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] + cpy16bitValueInstructionVector();
    return;
}

void comp24::sbmFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] - cpy16bitValueMemory(cpyAddress());
    return;
}

void comp24::sbrFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] - registers[instructionVectorInner.bytes[2]];
    return;
}
void comp24::sbvFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] - cpy16bitValueInstructionVector();
    return;
}

void comp24::jpmFunction(void){
    pc = cpyAddress();
    return;
}

//0: Z
//1: L
//2: H
//3: C
void comp24::jpzFunction(void){
    if(cpuStates[(int)zCpuSate] == true) pc = cpyAddress();
    return;
}

void comp24::jpcFunction(void){
    if(cpuStates[(int)c] == true) pc = cpyAddress();
    return;
}

void comp24::jpnFunction(void){
    if(cpuStates[(int)n] == true) pc = cpyAddress();
    return;
}

void comp24::jphFunction(void){
    if(cpuStates[(int)h] == true) pc = cpyAddress();
    return;
}

void comp24::cmpFunction(void){
    int16_t ref = (int16_t)registers[instructionVectorInner.bytes[1]] - (int16_t)registers[instructionVectorInner.bytes[2]];
    if(ref == 0) cpuStates[(int)zCpuSate] = true;
    if(ref < 0) cpuStates[(int)n] = true;
    if(ref > 1) cpuStates[(int)h] = true;
    return;
}

void comp24::brkFunction(void){
    gotToBreak = true;
    return;
}

void comp24::lsfFunction(void){
    registers[instructionVectorInner.bytes[1]] = registers[instructionVectorInner.bytes[1]] << 1;
    return;
}

void comp24::rsfFunction(void){
    registers[instructionVectorInner.bytes[1]] = registers[instructionVectorInner.bytes[1]] >> 1;
    return;
}

void comp24::inrFunction(void){
    registers[instructionVectorInner.bytes[1]] = ~registers[instructionVectorInner.bytes[1]];
    return;
}

void comp24::incFunction(void){
    registers[instructionVectorInner.bytes[1]]++;
    return;
}

void comp24::decFunction(void){
    registers[instructionVectorInner.bytes[1]]--;
    return;
}

void comp24::mumFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] * cpy16bitValueMemory(cpyAddress());
    return;
}

void comp24::murFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] * registers[instructionVectorInner.bytes[2]];
    return;
}

void comp24::muvFunction(void){
    registers[(int)a] = registers[instructionVectorInner.bytes[1]] * cpy16bitValueInstructionVector();
    return;
}

void comp24::dimFunction(void){
    registers[(int)a] = (uint16_t)(registers[instructionVectorInner.bytes[1]] / cpy16bitValueMemory(cpyAddress()));
    return;
}

void comp24::dirFunction(void){
    registers[(int)a] = (uint16_t)(registers[instructionVectorInner.bytes[1]] / registers[instructionVectorInner.bytes[2]]);
    return;
}

void comp24::divFunction(void){
    registers[(int)a] = (uint16_t)(registers[instructionVectorInner.bytes[1]] / cpy16bitValueInstructionVector());
    return;
}

bool comp24::getGotToBreak(void){
    return  gotToBreak;
}
