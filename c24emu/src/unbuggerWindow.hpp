#ifndef UNBUGGERWINDOW_HPP_
#define UNBUGGERWINDOW_HPP_

#include "emulatorCore.hpp"

void unbuggerWindow(comp24* cpuP, bool* openUnBuggerWindowP, bool canEmulatorStart);

#endif
