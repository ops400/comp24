#ifndef FILEMANAGER_HPP_
#define FILEMANAGER_HPP_
#include <cstddef>
#include "emulatorCore.hpp"
#include <cstdint>
#include <fstream>
#include <vector>

#define DUMMY_QMAX_FILE_SIZE COMP24_MEM_MAX+20

class FileManagerEx{
    public:
        std::vector<uint8_t> openROM(std::string filePath); // for getting the compiled sca
        // 2 some error happend at file writting
        // 1 could not open file
        // 0 file opened
        int saveMemDump(std::string filePath, std::vector<uint8_t> wholeMemory); // dumping the memory someWhere
};

#endif
