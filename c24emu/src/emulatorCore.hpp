#ifndef EMULATORCORE_HPP_
#define EMULATORCORE_HPP_
#define COMP24_MEM_MAX 16777216
#define COMP24_BIT_MAX 16777215
#include <cstdint>
#include <sys/types.h>

enum instructions{
    nop,
    ldm,
    ldr,
    str,
    stv,
    adm,
    adr,
    adv,
    sbm,
    sbr,
    sbv,
    jpm,
    jpz,
    jpc,
    jpn,
    jph,
    cmp,
    brk,
    lsf,
    rsf,
    inr,
    inc,
    dec,
    mum,
    mur,
    muv,
    dim,
    dir,
    divC24 // naming change because of stupid C naming (stdlib.h's div)
}; // and yes I copied from SCA's enum

enum cpuStatesEnum{
    zCpuSate,
    n,
    h,
    c
};

enum registersEnum{
    a,
    w,
    x,
    y,
    zRegister,
    pior
};

typedef struct instructionVector{
    uint8_t bytes[5];
} instructionVector;

typedef struct bit24{
    uint8_t bytes[3];
} bit24;

typedef struct bit16{
    uint8_t bytes[2];
} bit16;

class comp24{
    public:
        // 2 mega bytes (or 16 mega bits)
        // for the entire comp24 memory space
        // uint8_t memory[COMP24_MEM_MAX];
        uint8_t* memory;
        // define the following fuckers in emulatorCore.cpp
        comp24(uint8_t* initalRom, uint32_t initalRomLength);
        ~comp24(void);
        uint16_t getRegister(registersEnum registerToGet);
        bool getCpuState(cpuStatesEnum cpuStateToGet);
        uint32_t getPc(void);
        instructionVector getCurrentInstuctionVector(void);
        uint32_t getJumpPinAddress(void);
        uint16_t getPioConfig(void);
        uint16_t getPioState(void);
        uint8_t getMio(void);
        bool getMemoryAccessPin(void);
        instructions getCurrentInstruction(void);
        bool getGotToBreak(void);
        // true  :the attempt to set pc was successful
        // false :fucking failure
        void setRegister(registersEnum registerToSet, uint16_t value);
        void setCpuState(cpuStatesEnum cpuStateToSet, bool value);
        bool setPc(uint32_t address);
        void setInstructionVector(instructionVector instructionVectorInput);
        bool setJumpPinAddress(uint32_t address);
        void setPioConfig(uint16_t value);
        void setPioStates(uint16_t value);

        // advances PC to the next instruction vector position
        void instructionHandler();
        void advancePc(void);
    private:
        instructions currentInstruction;
        // true when get to break
        bool gotToBreak;
        // the program counter
        uint32_t pc;
        //0: Z
        //1: L
        //2: N
        //3: C
        bool cpuStates[4];
        //0: A
        //1: W
        //2: X
        //3: Y
        //4: Z
        //5: PIOR
        uint16_t registers[6];
        // the basic 5 bytes instruction comp24 vector
        // will copy the current 5 bytes from memory relative to the PC
        instructionVector instructionVectorInner;
        // stores the address that the jump pin will jump to
        uint32_t jumpPinAddress;
        // loads the 4th and 5th byte to configure the pio behavior
        // could have used a 16 bool array, the same goes for pioState
        uint16_t pioConfig;
        // it's involved with the stupid way that the last two bytes work
        uint16_t pioSates;
        // shows what are the bits of the MIO
        uint8_t mio;
        // true when writing to memory and false when reading from memory
        bool memoryAccessPin;

        void instructionVectorGetFromMemory(void);
        void setCurrentInstruction(void);
        // gets the address from the instruction vector
        uint32_t cpyAddress();
        uint16_t cpy16bitValueInstructionVector(); // for adv likes
        uint16_t cpy16bitValueMemory(uint32_t address); // for adm likes

        void ldmFunction(void);
        void ldrFunction(void);
        void strFunction(void);
        void stvFunction(void);
        void admFunction(void);
        void adrFunction(void);
        void advFunction(void);
        void sbmFunction(void);
        void sbrFunction(void);
        void sbvFunction(void);
        void jpmFunction(void);
        void jpzFunction(void);
        void jpcFunction(void);
        void jpnFunction(void);
        void jphFunction(void);
        void cmpFunction(void);
        void brkFunction(void);
        void lsfFunction(void);
        void rsfFunction(void);
        void inrFunction(void);
        void incFunction(void);
        void decFunction(void);
        void mumFunction(void);
        void murFunction(void);
        void muvFunction(void);
        void dimFunction(void);
        void dirFunction(void);
        void divFunction(void);
};

// both expect unformatted little endian.
// so lets say the value is A901
// byte0 will be 01 and not A9.
// this will apply to the following functions.
uint32_t brknDwn24BitToUint32(bit24 value);
uint16_t brknDwn16BitToUint16(bit16 value);

#endif
