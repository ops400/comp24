#ifndef SILENT_HPP_
#define SILENT_HPP_
#include <string>

void silentMode(std::string filePath, std::string memoryDumpFilePath, bool dumpMemory);

#endif
