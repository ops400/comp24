// #include <raylib.h>
// #include "deps/rlImGui.h"
// #include "deps/imgui.h"
#include "main.hpp"
#include "deps/imGuiFileDialog/ImGuiFileDialog.h"
#include "emulatorCore.hpp"
#include "fileManager.hpp"
#include "deps/imgui/imgui.h"
#include "deps/rlImGui/rlImGui.h"
#include "deps/imGuiFileDialog/ImGuiFileDialogConfig.h"
#include "silent.hpp"
#include "superVerbose.hpp"
#include "unbuggerWindow.hpp"
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <raylib.h>
#include <string>
#include <vector>

int main(int argc, char** argv){
    printf("Welcome to the COMP 24 emulator!\nFor help type c24emu -h\n");
    if(argc > 1){
        if(strcmp(argv[1], "-h") == 0){
            printf("c24emu [flag type 1] [file path] [flag type 2] [memory dump path]\nExcept for help(this one)\n\n");
            printf("Type 1 flag(s):\n");
            printf("--silent use when you want to emulate something without a terminal output (terminal only)\n");
            printf("--superverbose shows almost everything that happens while emulating (termial only)\n");
            printf("--gui creates the fancy graphical interface\n");
            printf("\n");
            printf("Type 2 flag(s):\n");
            printf("--dump for the the entire memory\n");
            return 0;
        }
        bool dumpMemory = false;
        std::string memoryDumpPath;
        if(argc >= 4){
            if(strcmp(argv[3], "--dump") == 0)
                dumpMemory = true;
            if(argc == 5){
                memoryDumpPath = std::string(argv[4]);
                // TEMP DEBUG
                printf("memoryDumpPath: %s\n", memoryDumpPath.c_str());
                // TEMP DEBUG
                if(memoryDumpPath == "" || memoryDumpPath == "."|| memoryDumpPath == ".." || memoryDumpPath == "/"){
                    printf("Bad memory dump path!\n");\
                    exit(2);
                }
            }
            else{
                printf("You need to put a path for the memory dump file\n");
                exit(1);
            }
        }
        if(strcmp(argv[1], "--silent") == 0){
            printf("Starting silent mode\n");
            silentMode(std::string(argv[2]), memoryDumpPath, dumpMemory);
            return 0;
        }
        if(strcmp(argv[1], "--superverbose") == 0){
            printf("Starting \"super verbose\" mode\n");
            superVerbose(std::string(argv[2]), memoryDumpPath, dumpMemory);
            return 0;
        }
        if(strcmp(argv[1], "--gui") == 0){
            printf("Starting Graphical User Interface(GUI) mode\n");
            return 0;
        }
    }

    return 0;
}
