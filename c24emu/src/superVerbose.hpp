#ifndef SUPERVERBOSE_HPP_
#define SUPERVERBOSE_HPP_
#include <string>

void superVerbose(std::string filePath, std::string memoryDumpFilePath, bool dumpMemory);

#endif
