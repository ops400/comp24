#include "silent.hpp"
#include "emulatorCore.hpp"
#include "fileManager.hpp"
#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <vector>

const std::string stringRegsiters[6] = {"A", "W", "X", "Y", "Z", "PIOR"};

void silentMode(std::string filePath, std::string memoryDumpFilePath, bool dumpMemory){
    printf("Reading file: %s\n", filePath.c_str());
    FileManagerEx fileManager;
    std::vector<uint8_t> initialRomVec = fileManager.openROM(filePath);
    uint8_t initialRom[initialRomVec.size()];
    switch(initialRomVec.size()){
        case 0:
            printf("The rom that you have loaded, seems to empty\nInitial rom size: %lu\n", initialRomVec.size());
            break;
        default:
        std::copy(initialRomVec.begin(), initialRomVec.end(), initialRom);
    }

    size_t initialRomSize = sizeof(initialRom)/sizeof(initialRom[0]);

    // TEMP DEBUG
    printf("sizeof(initalRom)/sizeof(initalRom[0]): %lu\n", sizeof(initialRom)/sizeof(initialRom[0]));
    printf("Initial rom size: %lu\n", initialRomSize);
    // TEMP DEBUG

    comp24 cpu(initialRom, initialRomSize);
    initialRomVec.clear();

    do{
        cpu.instructionHandler();
    }while(cpu.getPc()+1 < initialRomSize && !cpu.getGotToBreak());

    for(uint8_t i = 0; i < 6; i++){
        printf("%s: %hu\n", stringRegsiters[i].c_str(), cpu.getRegister(static_cast<registersEnum>(i)));
    }

    if(dumpMemory){
        std::vector<uint8_t> wholeMemory(cpu.memory, cpu.memory + COMP24_MEM_MAX);
        fileManager.saveMemDump(memoryDumpFilePath, wholeMemory);
        wholeMemory.clear();
    }

    return;
}
