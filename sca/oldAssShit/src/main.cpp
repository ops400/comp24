#include <bits/types/FILE.h>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <vector>
#include <iostream>

#define N_INSTRUCTIONS 29
typedef uint8_t byte;
typedef enum instructions{
        nop,
        ldm,
        ldr,
        str,
        stv,
        adm,
        adr,
        adv,
        sbm,
        sbr,
        sbv,
        jpm,
        jpz,
        jpc,
        jpn,
        jph,
        cmp,
        brk,
        lsf,
        rsf,
        inr,
        inc,
        dec,
        mum,
        mur,
        muv,
        dim,
        dir,
        divC24 // naming change because of stupid C naming (stdlib.h's div)
}instructions;

typedef enum tokens{
        instruction,
        address,
        number,
        registerCall,
        empty
}tokens;

typedef enum registers{
    a,
    w,
    x,
    y,
    z,
    pior,
    emptyR
} registers;

const char symbols[3]{'#', '$', ' '};
const char* instructionsString[29] = {
"nop", 
"ldm",
"ldr",
"str",
"stv",
"adm",
"adr",
"adv",
"sbm",
"sbr",
"sbv",
"jpm",
"jpz",
"jpc",
"jpn",
"jph",
"cmp",
"brk",
"lsf",
"rsf",
"inr",
"inc",
"dec",
"mum",
"mur",
"muv",
"dim",
"dir",
"div"};
const char* registersString[6] = {"a", "w", "x", "y", "z", "pior"};
// converts the instruction position on the instructionsString to a valid instruction enum
instructions instructionThing(uint32_t index);
// clears a char array
void clearCharArray(char* s, uint16_t size);
// checks if the specified register is exists
bool validateRegisterChar(char s[4]);
// checks if the 8 bit value is valid
bool validate8BitChar(char s[3]);
// same as validate8BitChar but for 16 bit
bool validate16BitChar(char s[5]);
// same as validate8BitChar bit for 24 bit memory addresses
bool validateMemoryAddressChar(char s[7]);
// converts from the specified register to the corresponding register enum using registersString as a reference
registers registerDecider(char s[4]);
// the following functions are written hexadecimal to numbers
uint8_t hexToDec8bit(char s[3]);
uint16_t hexToDec16bit(char s[5]);
uint32_t hexToDec24bit(char s[7]);
// prints each char of a char array with a \n between them
void stupid(char* s, uint16_t size);
// my own version of a classic and simple power function
unsigned int myPow(uint16_t base, uint16_t power);
// inverts a char array
void inverter(char* s);
// converts a single number char (or char at that matter) to a number
uint8_t singleCharToInt(char c);
// breaks down bigger bits values to a various 8 bit values
uint8_t bit16BreakUp(uint16_t bytes, uint8_t index);
uint8_t bit24BreakUp(uint32_t bytes, uint8_t index);
// converts from a instruction from the instructions enum to a unsigned integer
uint8_t instructionToUint8(instructions instruction);
// converts from a register from the register enum to a unsigned integer
uint8_t registerToUint8(registers registerR);

int main(int argc, char** argv){
    FILE *fp;
    uint32_t nLines = 0;
    switch(argc){
        case 1:
            printf("Argument error\n");
            exit(0);
        case 2:
            printf("%s\n", argv[1]);
            fp = fopen(argv[1], "r");
            char temp[50];
            while(fgets(temp, 50, fp)){
                nLines++;
                printf("%s", temp);
            }
            printf("\n\n");
            fclose(fp);
            break;
        default:
            printf("Argument error\n");
            exit(0);
    }

    char fileContent[nLines][50];
    fp = fopen(argv[1], "r");
    for(uint32_t i = 0; i < nLines; i++){
        fgets(fileContent[i], 50, fp);
    }
    fclose(fp);
    for(uint32_t i = 0; i < nLines; i++) printf("%s", fileContent[i]);
 
    tokens treeOfTokens[nLines][3];
    instructions instructionOfEachLine[nLines];
    char tempInstructionName[4];
    uint8_t nOfRepetitions = 0;
    bool hasAlreadyFound = false;

    printf("nLines: %u\n", nLines);

    // gets the instruction
    for(uint32_t i = 0; i < nLines; i++){
        clearCharArray(tempInstructionName, sizeof(tempInstructionName));
        for(byte j = 0; j < 3; j++) tempInstructionName[j] = fileContent[i][j];
        tempInstructionName[3] = 0;
        for(byte j = 0; j < N_INSTRUCTIONS && !hasAlreadyFound; j++){
                // printf("strcmp: %d\n", strcmp(tempInstructionName, instructionsString[j]));
                // for(uint8_t k = 0; k < 4; k++){
                    // printf("tempInstructionName[%u]: %d\n", k, tempInstructionName[k]);
                    // printf("instructionsString[%u][%u]: %d\n", j, k, instructionsString[j][k]);
                // }
                // printf("nOfRepetitions: %u\n", nOfRepetitions);
                if(strcmp(tempInstructionName, instructionsString[j]) == 0){
                    treeOfTokens[i][0] = instruction;
                    instructionOfEachLine[i] = instructionThing(j);
                    // printf("instructionThing(%u) I: %u: %d\n", j, i, instructionThing(j));
                    nOfRepetitions = 0;
                    hasAlreadyFound = true;
                    // printf("HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIii!\n");
                }
                else if(!hasAlreadyFound) nOfRepetitions++;
                if(nOfRepetitions == N_INSTRUCTIONS){
                    printf("invalid instruction at line %u\n", i+1);
                    exit(0);
                }
        }
        hasAlreadyFound = false;
    }

    for(uint32_t i = 0; i < nLines; i++) printf("instructionOfEachLine[%u]: %d\n", i, instructionOfEachLine[i]);
    for(uint32_t i = 0; i < nLines; i++) printf("treeOfTokens[%u][0]: %d\n", i, treeOfTokens[i][0]);

    printf("Size temp: %lu\n", sizeof(tempInstructionName));

    registers registerTree[nLines][2];
    uint8_t   byteTree[nLines];
    uint16_t  bit16Tree[nLines];
    uint32_t  bit24Tree[nLines];

    for(uint32_t i = 0; i < nLines; i++){
        for(uint8_t j = 0; j < 2; j++) registerTree[i][j] = emptyR;
        byteTree[i] =  0x00;
        bit16Tree[i] = 0x0000;
        bit24Tree[i] = 0x00000000;
    }

    char validRegister[4];
    char valid8BitValue[3];
    char valid16BitValue[5];
    char validMemoryAddress[7];
    clearCharArray(validRegister, 4);
    clearCharArray(valid8BitValue, 3);
    clearCharArray(valid16BitValue, 5);
    clearCharArray(validMemoryAddress, 7);


    bool firstIsPior = false;
    uint8_t rightTimes = 0;
    for(uint32_t i = 0; i < nLines; i++){
        switch(instructionOfEachLine[i]){
        // no argument
        case nop:
        case brk:
            treeOfTokens[i][1] = empty;
            treeOfTokens[i][2] = empty;
            break;
        // one argument (register)
        case inc:
        case dec:
        case rsf:
        case lsf:
        case inr:
            if(fileContent[i][4] == 'p') for(uint8_t j = 0; j < 4; j++) validRegister[j] = fileContent[i][j+4];
            else validRegister[0] = fileContent[i][4];
            stupid(validRegister, 4);
            if(validateRegisterChar(validRegister)){
                treeOfTokens[i][1] = registerCall;
                treeOfTokens[i][2] = empty;
                registerTree[i][0] = registerDecider(validRegister);
            }
            else{
                printf("Some error at line %u\n", i+1);
                exit(0);
            }
            break;
        // one argument (memory address)
        case jpm:
        case jpz:
        case jpc:
        case jpn:
        case jph:
        // abc $FFFFFF
            for(uint8_t j = 0; j < 7; j++) validMemoryAddress[j] = fileContent[i][j+4];
            stupid(validMemoryAddress, 7);
            if(validateMemoryAddressChar(validMemoryAddress)){
                treeOfTokens[i][1] = address;
                treeOfTokens[i][2] = empty;
                bit24Tree[i] = hexToDec24bit(validMemoryAddress);
                // place here the thing
            }
            else{
                printf("Some error at line %u\n", i+1);
                exit(0);
            }
            break;
        // two argument (register + memory address)
        case ldm:
        case str:
        case adm:
        case sbm:
        case dim:
        case mum:
            if(fileContent[i][4] == 'p'){
                for(uint8_t j = 0; j < 4; j++) validRegister[j] = fileContent[i][j+4];
                firstIsPior = true;
            }
            else validRegister[0] = fileContent[i][4];
            // 0123456789
            // acx w $888888
            // 0123456789
            // acx pior $888888
            switch(firstIsPior){
                case false:
                    for(uint8_t j = 0; j < 7; j++) validMemoryAddress[j] = fileContent[i][j+6];
                    break;
                case true:
                    for(uint8_t j = 0; j < 7; j++) validMemoryAddress[j] = fileContent[i][j+9];
                    break;
            }
            stupid(validRegister, 4);
            stupid(validMemoryAddress, 7);
            printf("VRC: %u\nVMC: %u\n", validateRegisterChar(validRegister), validateMemoryAddressChar(validMemoryAddress));
            if(validateRegisterChar(validRegister) && validateMemoryAddressChar(validMemoryAddress)){
                treeOfTokens[i][1] = registerCall;
                treeOfTokens[i][2] = address;
                registerTree[i][0] = registerDecider(validRegister);
                bit24Tree[i] = hexToDec24bit(validMemoryAddress);
                // place here the thing
            }
            else{
                printf("Some error at line %u\n", i+1);
                exit(0);
            }
            break;
        // two argument (register + register)
        case ldr:
        case adr:
        case sbr:
        case cmp:
        case dir:
        case mur:
            if(fileContent[i][4] == 'p'){
                for(uint8_t j = 0; j < 4; j++) validRegister[j] = fileContent[i][j+4];
                firstIsPior = true;
            }
            else validRegister[0] = fileContent[i][4];
            stupid(validRegister, 4);
            if(validateRegisterChar(validRegister)){
                rightTimes++;
                registerTree[i][0] = registerDecider(validRegister);
            }
            // 0123456789
            // abc pior w
            clearCharArray(validRegister, 4);
            if(firstIsPior){
                if(fileContent[i][9] == 'p') for(uint8_t j = 0; j < 4; j++) validRegister[j] = fileContent[i][j+9];
                else validRegister[0] = fileContent[i][9];
            }
            // 0123456
            // abc w pior
            else{
                if(fileContent[i][6] == 'p') for(uint8_t j = 0; j < 4; j++) validRegister[j] = fileContent[i][j+6];
                else validRegister[0] = fileContent[i][6];
            }
            stupid(validRegister, 4);
            if(validateRegisterChar(validRegister)){
                rightTimes++;
                registerTree[i][1] = registerDecider(validRegister);
            }
            if(rightTimes == 2){
                treeOfTokens[i][1] = registerCall;
                treeOfTokens[i][2] = registerCall;
            }
            else{
                printf("RTs: %u\n", rightTimes);
                printf("Some error at line %u\n", i+1);
                exit(0);
            }
            break;
        // two argument (register + 16 bit value)
        case adv:
        case sbv:
        case muv:
        case divC24:
            // 0123456789
            // abc w #FFFF
            // abc pior #FFFF
            if(fileContent[i][4] == 'p'){
                for(uint8_t j = 0; j < 4; j++) validRegister[j] = fileContent[i][j+4];
                firstIsPior = true;
            }
            else validRegister[0] = fileContent[i][4];
            if(firstIsPior){
                for(uint8_t j = 0; j < 5; j++) valid16BitValue[j] = fileContent[i][j+9];
            }
            else{
                for(uint8_t j = 0; j < 5; j++) valid16BitValue[j] = fileContent[i][j+6];
            }
            if(validateRegisterChar(validRegister) && validate16BitChar(valid16BitValue)){
                treeOfTokens[i][1] = registerCall;
                treeOfTokens[i][2] = number;
                registerTree[i][0] = registerDecider(validRegister);
                bit16Tree[i] = hexToDec16bit(valid16BitValue);
            }
            else{
                printf("Some error at line %u\n", i+1);
                exit(0);
            }
            break;
        // two argument (8 bit value + memory address)
        case stv:
            // 012345678
            // abc #FF $AAFFCC
            for(uint8_t j = 0; j < 3; j++) valid8BitValue[j] = fileContent[i][j+4];
            for(uint8_t j = 0; j < 7; j++) validMemoryAddress[j] = fileContent[i][j+8];
            if(validate8BitChar(valid8BitValue) && validateMemoryAddressChar(validMemoryAddress)){
                treeOfTokens[i][1] = number;
                treeOfTokens[i][2] = address;
                byteTree[i] = hexToDec8bit(valid8BitValue);
                bit24Tree[i] = hexToDec24bit(validMemoryAddress);
            }
            else{
                printf("Some error at line %u\n", i+1);
                exit(0);
            }
            break;
        }
        clearCharArray(validRegister, 4);
        clearCharArray(valid8BitValue, 3);
        clearCharArray(valid16BitValue, 5);
        clearCharArray(validMemoryAddress, 7);
        firstIsPior = false;
        rightTimes = 0;
    }

    for(uint32_t i = 0; i < nLines; i++){
        printf("Line: %u\n", i);
        printf("    tokens: %d %d %d\n", treeOfTokens[i][0], treeOfTokens[i][1], treeOfTokens[i][2]);
        printf("    instruction: %u\n", instructionOfEachLine[i]);
        printf("    registers: ");
        for(uint8_t j = 0; j < 2; j++) printf("%u ", registerTree[i][j]);
        printf("\n");
        printf("    8 bits: %x\n", byteTree[i]);
        printf("    16 bits: %x\n", bit16Tree[i]);
        printf("    24 bits: %x\n", bit24Tree[i]);
    }

    uint8_t conv16[nLines][2];
    uint8_t conv24[nLines][3];
    for(uint32_t i = 0; i < nLines; i++){
        for(uint8_t j = 0; i < 2; i++) conv16[i][j] = 0x00;
        for(uint8_t k = 0; i < 3; i++) conv24[i][k] = 0x00;
    }


    // IDK WHAT THIS COMMENT MEANT!!!
    // a sleep deprived me wont get this right
    // but I'm getting there

    // executes the 16 and 24 bit break up to 8 bit
    for(uint32_t i = 0; i < nLines; i++){
        for(uint8_t j = 1; j < 3; j++){
            switch(treeOfTokens[i][j]){
                case address:
                    for(uint8_t k = 0; k < 3; k++){
                        if(bit24Tree[i] > 0){
                            conv24[i][k] = bit24BreakUp(bit24Tree[i], k);   
                            // printf("BIT24TREE[%u]=%x\n", i, bit24Tree[i]);
                        }
                        else conv24[i][k] = 0;
                        printf("conv24[%u][%u]=%x\n", i, k, conv24[i][k]);
                    }
                    break;
                case number:
                    // I mean... someone could do a stv #00 $FFFFFF,
                    // the exemples has this, or an adv w #0000, but well
                    // a conflict lets see. 
                    for(uint8_t k = 0; k < 2; k++){
                        if(bit16Tree[i] > 0){
                            conv16[i][k] = bit16BreakUp(bit16Tree[i], k);
                        }
                        else conv16[i][k] = 0;
                    }
                    break;
                case registerCall:
                    break;
                case empty:
                    break;
                default:
                    printf("Something really wrong must have happen.\n");
                    exit(0);
            }
        }
    }

    for(uint32_t i = 0; i < nLines; i++){
        printf("16 bit:");
        for(uint8_t j = 0; j < 2; j++) printf("%x ", conv16[i][j]);
        printf("\n24 bit:");
        for(uint8_t j = 0; j < 3; j++) printf("%x ", conv24[i][j]);
        printf("\n");
    }
    printf("NEEEEEEEEEEEEEEEEEEEW\n");

    for(uint32_t i = 0; i < nLines; i++){
        if(treeOfTokens[i][2] != address && treeOfTokens[i][1] != address){
            for(uint8_t j = 0; j < 3; j++) conv24[i][j] = 0;
        }
        if(treeOfTokens[i][2] != number){
            for(uint8_t j = 0; j < 2; j++) conv16[i][j] = 0;
        }
    }

    for(uint32_t i = 0; i < nLines; i++){
        printf("16 bit:");
        for(uint8_t j = 0; j < 2; j++) printf("%x ", conv16[i][j]);
        printf("\n24 bit:");
        for(uint8_t j = 0; j < 3; j++) printf("%x ", conv24[i][j]);
        printf("\n");
    }

    uint8_t outputFileContent[nLines+1][5];
    // cleans outputFileContent
    for(uint32_t i = 0; i < nLines+1; i++){
        for(uint8_t j = 0; j < 5; j++){
            outputFileContent[i][j] = 0;
        }
    }

    // typedef enum tokens{
        // instruction,
        // address,
        // number,
        // registerCall,
        // empty
    // }tokens;

    // 0;1
    // 1;2
    // 2;3
    // 3;4      two bytes spacing
    // 00 00 03 00 00

    for(uint32_t i = 1; i < nLines+1; i++){
        switch(instructionOfEachLine[i-1]){
            // no arguments
            case nop:
            case brk:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i-1]);
                break;
            // two arguments register with memory address
            case sbm:
            case adm:
            case ldm:
            case str:
            case mum:
            case dim:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i-1]);
                outputFileContent[i][1] = registerTree[i-1][0];
                for(uint8_t j = 0; j < 3; j++){
                    outputFileContent[i][j+2] = conv24[i-1][j];
                }
                break;
            // two arguments register with register
            case ldr:
            case cmp:
            case adr:
            case sbr:
            case dir:
            case mur:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i-1]);
                for(uint8_t j = 0; j < 2; j++){
                    outputFileContent[i][j+1] = registerTree[i-1][j];
                }
                break;
            // two arguments register and 16 bit value
            case adv:
            case sbv:
            case divC24:
            case muv:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i-1]);
                outputFileContent[i][1] = registerTree[i-1][0];
                for(uint8_t j = 0; j < 2; j++){
                    outputFileContent[i][j+2] = conv16[i-1][j];
                }
                break;
            // one argument memory address
            case jpm:
            case jpc:
            case jpn:
            case jph:
            case jpz:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i-1]);
                for(uint8_t j = 0; j < 3; j++){
                    outputFileContent[i][j+2] = conv24[i-1][j];
                }
                break;
            // one argument register
            case inc:
            case dec:
            case inr:
            case lsf:
            case rsf:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i-1]);
                outputFileContent[i][1] = registerTree[i-1][0];
                break;
            // two arguments 8 bit value and memory address
            case stv:
                outputFileContent[i][0] = instructionToUint8(instructionOfEachLine[i]);
                outputFileContent[i][1] = byteTree[i-1];
                for(uint8_t j = 0; j < 3; j++){
                    outputFileContent[i][j+2] = conv24[i-1][j];
                }
                break;
        }
    }

    for(uint32_t i = 0; i < nLines+1; i++){
        for(uint8_t j = 0; j < 5; j++) printf("%X ", outputFileContent[i][j]);
        printf("\n");
    }

    char outFileName[strlen(argv[1])];
    for(uint16_t i = 0; i < sizeof(outFileName); i++) outFileName[i] = '\0';
    strcpy(outFileName, argv[1]);
    inverter(outFileName);
    printf("%s\n", outFileName);
    outFileName[0] = 'n';
    outFileName[1] = 'i';
    outFileName[2] = 'b';
    inverter(outFileName);
    printf("%s\n", outFileName);
    fp = fopen(outFileName, "wb");
    for(uint32_t i = 0; i < nLines+1; i++){
        fwrite(outputFileContent[i], 1, 5, fp);
    }    
    printf("end\n");
    return 0;
}

instructions instructionThing(uint32_t index){
    switch(index){
        case 0:
            return nop;
            break;
        case 1:
            return ldm;
            break;
        case 2:
            return ldr;
            break;
        case 3:
            return str;
            break;
        case 4:
            return stv;
            break;
        case 5:
            return adm;
            break;
        case 6:
            return adr;
            break;
        case 7:
            return adv;
            break;
        case 8:
            return sbm;
            break;
        case 9:
            return sbr;
            break;
        case 10:
            return sbv;
            break;
        case 11:
            return jpm;
            break;
        case 12:
            return jpz;
            break;
        case 13:
            return jpc;
            break;
        case 14:
            return jpn;
            break;
        case 15:
            return jph;
            break;
        case 16:
            return cmp;
            break;
        case 17:
            return brk;
            break;
        case 18:
            return lsf;
            break;
        case 19:
            return rsf;
            break;
        case 20:
            return inr;
            break;
        case 21:
            return inc;
            break;
        case 22:
            return dec;
            break;
        case 23:
            return mum;
            break;
        case 24:
            return mur;
            break;
        case 25:
            return muv;
            break;
        case 26:
            return dim;
            break;
        case 27:
            return dir;
            break;
        case 28:
            return divC24;
            break;
        default:
            return nop;
    }
    return nop;
}

void clearCharArray(char* s, uint16_t length){
    for(uint16_t i = 0; i < length; i++) s[i] = 0;
    return;
}

bool validateRegisterChar(char s[4]){
    if(s[0] == 'p' && s[1] == 'i' && s[2] == 'o' && s[3] == 'r') return true;
    if(s[0] == 'a') return true;
    if(s[0] == 'w') return true;
    if(s[0] == 'x') return true;
    if(s[0] == 'y') return true;
    if(s[0] == 'z') return true;
    return false;
}
bool validate8BitChar(char s[3]){
    if(s[0] == '#'){
        uint8_t rightTimes = 0;
        for(uint8_t i = 1; i < 3; i++){
            if((s[i] > 47 && s[i] < 58) || (s[i] > 64 && s[i] < 91)) rightTimes++;
        }
        switch(rightTimes){
            case 2:
                return true;
            default:
                return false;
        }
    }
    else return false;
    return false;
}
bool validate16BitChar(char s[5]){
    if(s[0] == '#'){
        uint8_t rightTimes = 0;
        for(uint8_t i = 1; i < 5; i++){
            if((s[i] > 47 && s[i] < 58) || (s[i] > 64 && s[i] < 91)) rightTimes++;
        }
        switch(rightTimes){
            case 4:
                return true;
            default:
                return false;
        }
    }
    else return false;
    return false;
}
bool validateMemoryAddressChar(char s[7]){
    if(s[0] == '$'){
        uint8_t rightTimes = 0;
        for(uint8_t i = 1; i < 7; i++){
            if((s[i] > 47 && s[i] < 58) || (s[i] > 64 && s[i] < 91)) rightTimes++;
        }
        switch(rightTimes){
            case 6:
                return true;
            default:
                return false;
        }
    }
    else return false;
    return false;
}

void stupid(char* s, uint16_t size){
    for(uint16_t i = 0; i < size; i++) printf("%c", s[i]);
    printf("\n");
    return;
}

// const char* registersString[6] = {"a", "w", "x", "y", "z", "pior"};

registers registerDecider(char s[4]){
    uint8_t nRights = 0;
    if(s[0] == registersString[0][0]) return a;
    if(s[0] == registersString[1][0]) return w;
    if(s[0] == registersString[2][0]) return x;
    if(s[0] == registersString[3][0]) return y;
    if(s[0] == registersString[4][0]) return z;
    for(uint8_t i = 0; i < 4; i++) if(s[i] == registersString[5][i]) nRights++;
    if(nRights == 4) return pior;

    return emptyR;
}

unsigned int myPow(uint16_t base, uint16_t power){
    unsigned int res;
    switch(power){
        case 0:
            return 1;
            break;
        case 1:
            return base;
            break;
        default:
            res = 1;
            for(uint16_t i = 0; i < power; i++) res = res * base;
    }
    return res;
}

void inverter(char* s){
    char temp;
    uint8_t start = 0;
    uint8_t end = strlen(s)-1;
    while(start < end){
        temp = s[end];
        s[end] = s[start];
        s[start] = temp;
        start++;
        end--;
    }
    return;
}

uint8_t singleCharToInt(char c){
    uint8_t finalNum = 0;
    if(c < 48) return 0;
    finalNum = c - 48;
    return finalNum;
}

uint8_t hexToDec8bit(char s[3]){
    char sCopy[3];
    for(uint8_t i = 0; i < 3; i++) sCopy[i] = s[i];
    uint8_t finalNum = 0;
    inverter(sCopy);
    for(uint8_t i = 0; i < 2; i++){
        switch(sCopy[i]){
            case 'A':
                finalNum += 10 * myPow(16, i);
                break;
            case 'B':
                finalNum += 11 * myPow(16, i);
                break;
            case 'C':
                finalNum += 12 * myPow(16, i);
                break;
            case 'D':
                finalNum += 13 * myPow(16, i);
                break;
            case 'E':
                finalNum += 14 * myPow(16, i);
                break;
            case 'F':
                finalNum += 15 * myPow(16, i);
                break;
            default:
                finalNum += singleCharToInt(sCopy[i]) * myPow(16, i);
        }
    }
    printf("8bit finalNum: %x\n", finalNum);
    return finalNum;
}

uint16_t hexToDec16bit(char s[5]){
    char sCopy[5];
    for(uint8_t i = 0; i < 5; i++) sCopy[i] = s[i];
    uint16_t finalNum = 0;
    inverter(sCopy);
    for(uint8_t i = 0; i < 4; i++){
        switch(sCopy[i]){
            case 'A':
                finalNum += 10 * myPow(16, i);
                break;
            case 'B':
                finalNum += 11 * myPow(16, i);
                break;
            case 'C':
                finalNum += 12 * myPow(16, i);
                break;
            case 'D':
                finalNum += 13 * myPow(16, i);
                break;
            case 'E':
                finalNum += 14 * myPow(16, i);
                break;
            case 'F':
                finalNum += 15 * myPow(16, i);
                break;
            default:
                finalNum += singleCharToInt(sCopy[i]) * myPow(16, i);
        }
    }
    printf("16bit finalNum: %x\n", finalNum);
    return finalNum;
}

uint32_t hexToDec24bit(char s[7]){
    char sCopy[7];
    for(uint8_t i = 0; i < 7; i++) sCopy[i] = s[i];
    uint32_t finalNum = 0;
    inverter(sCopy);
    for(uint8_t i = 0; i < 6; i++){
        switch(sCopy[i]){
            case 'A':
                finalNum += 10 * myPow(16, i);
                break;
            case 'B':
                finalNum += 11 * myPow(16, i);
                break;
            case 'C':
                finalNum += 12 * myPow(16, i);
                break;
            case 'D':
                finalNum += 13 * myPow(16, i);
                break;
            case 'E':
                finalNum += 14 * myPow(16, i);
                break;
            case 'F':
                finalNum += 15 * myPow(16, i);
                break;
            default:
                finalNum += singleCharToInt(sCopy[i]) * myPow(16, i);
        }
    }
    printf("24bit finalNum: %x\n", finalNum);
    return finalNum;
}

uint8_t bit16BreakUp(uint16_t bytes, uint8_t index){
    uint16_t temp = 0;
    uint8_t conv = 0;
    // f8d3
    switch(index){
        case 0:    
            temp = bytes << 8;
            conv = temp >> 8;
            return conv;
            break;
        case 1:
            conv = bytes >> 8;
            return conv;
            break;
        default:
            return 0;
    }
    return 0;
}

uint8_t bit24BreakUp(uint32_t bytes, uint8_t index){
    // printf("BYTES: %x\n", bytes);
    uint32_t temp = 0;
    uint8_t conv = 0;
    // 32
    // 00f7a790
    switch(index){
        case 0:
            temp = bytes << 24;
            conv = temp >> 24;
            return conv;
            break;
        case 1:
            temp = bytes << 16;
            conv = temp >> 24;
            return conv;
            break;
        case 2:
            conv = bytes >> 16;
            return conv;
            break;
        default:
            return 0;
    }
    return 0;
}

uint8_t instructionToUint8(instructions instruction){
    // is there a better way for doing this?
    switch(instruction){
        case nop:
            return 0;
            break;
        case ldm:
            return 1;
            break;
        case ldr:
            return 2;
            break;
        case str:
            return 3;
            break;
        case stv:
            return 4;
            break;
        case adm:
            return 5;
            break;
        case adr:
            return 6;
            break;
        case adv:
            return 7;
            break;
        case sbm:
            return 8;
            break;
        case sbr:
            return 9;
            break;
        case sbv:
            return 10;
            break;
        case jpm:
            return 11;
            break;
        case jpz:
            return 12;
            break;
        case jpc:
            return 13;
            break;
        case jpn:
            return 14;
            break;
        case jph:
            return 15;
            break;
        case cmp:
            return 16;
            break;
        case brk:
            return 17;
            break;
        case lsf:
            return 18;
            break;
        case rsf:
            return 19;
            break;
        case inr:
            return 20;
            break;
        case inc:
            return 21;
            break;
        case dec:
            return 22;
            break;
        case mum:
            return 23;
            break;
        case mur:
            return 24;
            break;
        case muv:
            return 25;
            break;
        case dim:
            return 26;
            break;
        case dir:
            return 27;
            break;
        case divC24:
            return 28;
            break;
    }
    return 0;
}

// typedef enum registers{
    // a,
    // w,
    // x,
    // y,
    // z,
    // pior,
    // emptyR
// } registers;

uint8_t registerToUint8(registers registerR){
    switch(registerR){
        case a:
            return 0;
            break;
        case w:
            return 1;
            break;
        case x:
            return 2;
            break;
        case y:
            return 3;
            break;
        case z:
            return 4;
            break;
        case pior:
            return 5;
            break;
        case emptyR:
            return 6;
            break;
        default:
            return 6;
    }
    return 0;
}
