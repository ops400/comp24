#ifndef FILEHANDLER_HPP_
#define FILEHANDLER_HPP_
#include <cstdint>

// forTextOnly
class fileHandler{
    public:
        uint8_t** INPUTfileContent;
        uint8_t* OUTPUTfileContent;

        uint16_t* INPUTnumberOfElementsByRow;

        // open input file
        fileHandler(char* fileName);
        // frees all memory in this 
        ~fileHandler(void);
        uint8_t** getFileContent(void);

        void setOutputFileContent(uint8_t*);
        void createFile(char* fileName);
    private:
        char* INPUTfileName;
        char* OUTPUTfileName;
        uint64_t fileSize = 0;
};

#endif