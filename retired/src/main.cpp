#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#define MAX_TOKEN_NUMBER_THING 26

// the declarations of these neat functions
uint16_t stringToUint16(std::string s);
void reverseCharArray(char* c, unsigned int start, unsigned int end);
unsigned int iIndex(std::string s);

int main(int argc, char** argv){
    if(argc == 2){
        // get file
        std::ifstream fileIn;
        fileIn.open(argv[1]);
        std::vector<std::string> stringForParsing;
        std::string strTemp;
        if(fileIn.is_open()){
            while(std::getline(fileIn, strTemp)){
                stringForParsing.push_back(strTemp);
            }
        }
        fileIn.close();
        for(unsigned int i = 0; i < stringForParsing.size(); i++){
            printf("i: %d; %s\n", i, stringForParsing[i].c_str());
        }
        printf("\n");

        // tokens definition
        typedef enum tokenTypes{
            nxPtr,
            prPtr,
            plus,
            minus,
            on,
            oc,
            nxn,
            compj,
            in,
            no,
            comp, // I should rename it and compj to equal and equalj, but to lazy to do it rn  
            btj,
            bfj,
            stop,
            prBool, // debug pursues
            add,
            sub, // countion with unsigned
            mult,
            div,
            idiv,
            jmp,
            hcomp,
            lcomp,
            hecomp,
            lecomp,
            ncomp,
            number
        } tokenTypes;
        // for comparing the stringForParsing and tokenizing it
        const char numbers[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        const char* tokenStrs[MAX_TOKEN_NUMBER_THING];
        tokenStrs[0] = ">";
        tokenStrs[1] = "<";
        tokenStrs[2] = "+";
        tokenStrs[3] = "-";
        tokenStrs[4] = "on";
        tokenStrs[5] = "oc";
        tokenStrs[6] = "nxn";
        tokenStrs[7] = "compj";
        tokenStrs[8] = "in";
        tokenStrs[9] = "no";
        tokenStrs[10] = "comp";
        tokenStrs[11] = "btj";
        tokenStrs[12] = "bfj";
        tokenStrs[13] = "stop";
        tokenStrs[14] = "prbool";
        tokenStrs[15] = "add";
        tokenStrs[16] = "sub";
        tokenStrs[17] = "mult";
        tokenStrs[18] = "div";
        tokenStrs[19] = "idiv";
        tokenStrs[20] = "jmp";
        tokenStrs[21] = "hcomp";
        tokenStrs[22] = "lcomp";
        tokenStrs[23] = "hecomp";
        tokenStrs[24] = "lecomp";
        tokenStrs[25] = "ncomp";
        std::vector<tokenTypes> instructions;
        char littleHolderForComparing[100];
        for(unsigned int i = 0; i < sizeof(littleHolderForComparing); i++) littleHolderForComparing[i] = 0;
        
        // the actual stringForParsing tokenizing code
        for(unsigned int i = 0; i < stringForParsing.size(); i++){
            strcpy(littleHolderForComparing, stringForParsing[i].c_str());
            for(unsigned int j = 0; j < MAX_TOKEN_NUMBER_THING; j++){
                if(strcmp(littleHolderForComparing, tokenStrs[j]) == 0){
                    switch(j){
                        case 0:
                            instructions.push_back(nxPtr);
                            break;
                        case 1:
                            instructions.push_back(prPtr);
                            break;
                        case 2:
                            instructions.push_back(plus);
                            break;
                        case 3:
                            instructions.push_back(minus);
                            break;
                        case 4:
                            instructions.push_back(on);
                            break;
                        case 5:
                            instructions.push_back(oc);
                            break;
                        case 6:
                            instructions.push_back(nxn);
                            break;
                        case 7:
                            instructions.push_back(compj);
                            break;
                        case 8:
                            instructions.push_back(in);
                            break;
                        case 9:
                            instructions.push_back(no);
                            break;
                        case 10:
                            instructions.push_back(comp);
                            break;
                        case 11:
                            instructions.push_back(btj);
                            break;
                        case 12:
                            instructions.push_back(bfj);
                            break;
                        case 13:
                            instructions.push_back(stop);
                            break;
                        case 14:
                            instructions.push_back(prBool);
                            break;
                        case 15:
                            instructions.push_back(add);
                            break;
                        case 16:
                            instructions.push_back(sub);
                            break;
                        case 17:
                            instructions.push_back(mult);
                            break;
                        case 18:
                            instructions.push_back(div);
                            break;
                        case 19:
                            instructions.push_back(idiv);
                            break;
                        case 20:
                            instructions.push_back(jmp);
                            break;
                        case 21:
                            instructions.push_back(hcomp);
                            break;
                        case 22:
                            instructions.push_back(lcomp);
                            break;
                        case 23:
                            instructions.push_back(hecomp);
                            break;
                        case 24:
                            instructions.push_back(lecomp);
                            break;
                        case 25:
                            instructions.push_back(ncomp);
                            break;
                        default:
                        ;
                    }
                }
                else if(j == 0){
                    for(unsigned int k = 0; k < sizeof(numbers); k++){
                        if(littleHolderForComparing[0] == numbers[k]) instructions.push_back(number);
                    }
                }
            }
        }
        //debug
        printf("\n\n");
        for(unsigned int i = 0; i < instructions.size(); i++) printf("%d\n", instructions[i]);
        printf("size of parse string: %lu\nsize of instructions: %lu\n", stringForParsing.size(), instructions.size());
        
        // getting the size of the stack
        unsigned int numberOfItemsInTheStack = 1;
        for(unsigned int i = 0; i < instructions.size(); i++){
            if(instructions[i] == nxPtr) numberOfItemsInTheStack++;
        }
        printf("number o items in the stack: %d\n", numberOfItemsInTheStack);

        bool previousBooleanComparison = false;
        uint16_t stack[numberOfItemsInTheStack];
        for(unsigned int i = 0; i < numberOfItemsInTheStack; i++) stack[i] = 0;
        unsigned int stackPos = 0;

        // the interpreter
        bool wasPrevNxn = false;
        bool wasPrevCompj = false;
        bool wasPrevBtj = false;
        bool wasPrevBfj = false;
        bool wasPrevJmp = false;
        for(unsigned int i = 0; i < instructions.size(); i++){
            switch(instructions[i]){
                case nxPtr:
                    stackPos++;
                    break;
                case prPtr:
                    if(stackPos > 0) stackPos--;
                    if(stackPos == 0) stackPos = 0;
                    break;
                case plus:
                    stack[stackPos]++;
                    break;
                case minus:
                    stack[stackPos]--;
                    break;
                case on:
                    printf("%d\n", stack[stackPos]);
                    break;
                case oc:
                    printf("%c", stack[stackPos]);
                    break;
                case nxn:
                    wasPrevNxn = true;
                    break;
                case compj:
                    wasPrevCompj = true;
                    break;
                case in:
                    std::cin >> stack[stackPos];
                    break;
                case number:
                    if(wasPrevNxn){
                        stack[stackPos] = stringToUint16(stringForParsing[i]);
                        wasPrevNxn = false;
                    }
                    if(wasPrevCompj){
                        if(stack[stackPos] == stack[stackPos+1]){
                            i = iIndex(stringForParsing[i]);
                        }
                        wasPrevCompj = false;
                    }
                    if(wasPrevBtj){
                        i = iIndex(stringForParsing[i]);
                        wasPrevBtj = false;
                    }
                    if(wasPrevBfj){
                        i = iIndex(stringForParsing[i]);
                        wasPrevBfj = false;
                    }
                    if(wasPrevJmp){
                        i = iIndex(stringForParsing[i]);
                        wasPrevJmp = false;
                    }
                    break;
                case no:
                    break;
                case comp:
                    previousBooleanComparison = stack[stackPos] == stack[stackPos+1];
                    break;
                case btj:
                    if(previousBooleanComparison) wasPrevBtj = true;
                    break;
                case bfj:
                    if(!previousBooleanComparison) wasPrevBfj = true;
                    break;
                case stop:
                    exit(0);
                    break;
                case prBool:
                    printf("%d\n", previousBooleanComparison);
                    break;
                case add:
                    stack[stackPos] = stack[stackPos] + stack[stackPos+1];
                    break;
                case sub:
                    stack[stackPos] = stack[stackPos] - stack[stackPos+1];
                    break;
                case mult:
                    stack[stackPos] = stack[stackPos] * stack[stackPos+1];
                    break;
                case div:
                    stack[stackPos] = stack[stackPos] / stack[stackPos+1];
                    break;
                case idiv:
                    stack[stackPos] = stack[stackPos] % stack[stackPos+1];
                    break;
                case jmp:
                    wasPrevJmp = true;
                    break;
                case hcomp:
                    previousBooleanComparison = stack[stackPos] > stack[stackPos+1];
                    break;
                case lcomp:
                    previousBooleanComparison = stack[stackPos] < stack[stackPos+1];
                    break;
                case hecomp:
                    previousBooleanComparison = stack[stackPos] >= stack[stackPos+1];
                    break;
                case lecomp:
                    previousBooleanComparison = stack[stackPos] <= stack[stackPos+1];
                    break;
                case ncomp:
                    previousBooleanComparison =  stack[stackPos] != stack[stackPos+1];
                    break;
            }
        }
    }
    else{
        printf("Please give me a file\n");
        exit(0);
    }

    return 0;
}

// the definitions of those neat functions
uint16_t stringToUint16(std::string s){
    uint16_t value = 0;
    char c[s.length()];
    strcpy(c, s.c_str());
    reverseCharArray(c, 0, strlen(c)-1);
    for(unsigned int i = 0; i < strlen(c); i++){
        value += (int)(c[i] - 48)*((int)pow(10, i));
    }
    return value;
}

void reverseCharArray(char* c, unsigned int start, unsigned int end){
    char temp;
    while(start < end){
        temp = c[start];
        c[start] = c[end];
        c[end] = temp;
        start++;
        end--;
    }
}

unsigned int iIndex(std::string s){
    unsigned int res = 0;
    if(stringToUint16(s) == 0){
        res = 0;
    }
    else {
        res = stringToUint16(s);
        res--;
    }
    return res;
}