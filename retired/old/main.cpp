#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#define FIND_LIMIT 18446744073709551615

uint16_t stringToUint16(std::string s);
void reverseCharArray(char* c, unsigned int start, unsigned int end);

int main(int argc, char** argv){
    std::ifstream file; file.open(argv[1]);
    std::string strTemp;
    std::vector<std::string> finalString;
    if(file.is_open()){
        while(std::getline(file, strTemp)){
            finalString.push_back(strTemp);
        }
    }

    for(unsigned int i = 0; i < finalString.size(); i++){
        printf("%s", finalString[i].c_str());
    }
    printf("\n");
    
    typedef enum tokens{
        nxPtr,
        prevPtr,
        on,
        oc,
        plus,
        minus,
        nxn,
        compj,
        in,
        noOp
    } tokens;
    
    const char* tokensString[9];
    tokensString[0] = ">";
    tokensString[1] = "<";
    tokensString[2] = "on";
    tokensString[3] = "oc";
    tokensString[4] = "+";
    tokensString[5] = "-";
    tokensString[6] = "nxn";
    tokensString[7] = "compj";
    tokensString[8] = "in";

    std::vector<unsigned int> whereAreTheEspecialLines;
    std::vector<tokens> tokensToRun;
    for(unsigned int i = 0; i < finalString.size(); i++){
        for(unsigned int j = 0; j < 9; j++){
            if(finalString[i].find(tokensString[j]) != FIND_LIMIT){
                switch(j){
                    case 0:
                        tokensToRun.push_back(nxPtr);
                        break;
                    case 1:
                        tokensToRun.push_back(prevPtr);
                        break;
                    case 2:
                        tokensToRun.push_back(on);
                        break;
                    case 3:
                        tokensToRun.push_back(oc);
                        break;
                    case 4:
                        tokensToRun.push_back(plus);
                        break;
                    case 5:
                        tokensToRun.push_back(minus);
                        break;
                    case 6:
                        tokensToRun.push_back(nxn);
                        break;
                    case 7:
                        tokensToRun.push_back(compj);
                        break;
                    case 8:
                        tokensToRun.push_back(in);
                        break;
                    default:
                        // tokensToRun.push_back(noOp);
                        ;
                }
            }
        }
        if(finalString[i].find(tokensString[0]) == FIND_LIMIT && finalString[i].find(tokensString[1]) == FIND_LIMIT && finalString[i].find(tokensString[2]) == FIND_LIMIT && 
        finalString[i].find(tokensString[3]) == FIND_LIMIT && finalString[i].find(tokensString[4]) == FIND_LIMIT && finalString[i].find(tokensString[5]) == FIND_LIMIT && 
        finalString[i].find(tokensString[6]) == FIND_LIMIT && finalString[i].find(tokensString[7]) == FIND_LIMIT && finalString[i].find(tokensString[8]) == FIND_LIMIT){
            printf("elsed on :%s\n", finalString[i].c_str());
            whereAreTheEspecialLines.push_back(i);
        }
    }

    for(unsigned int i = 0; i < whereAreTheEspecialLines.size(); i++) printf("%d\n", whereAreTheEspecialLines[i]);

    for(unsigned int i = 0; i < tokensToRun.size(); i++){
        printf("%d\n", tokensToRun[i]);
    }
    printf("\n");

    unsigned int numberOfStackItens = 1;
    for(unsigned int i = 0; i < tokensToRun.size(); i++){
        if(tokensToRun[i] == nxPtr) numberOfStackItens++;
    }

    uint16_t stack[numberOfStackItens+1];
    for(unsigned int i  = 0; i < numberOfStackItens; i++) stack[i] = 0;

    unsigned int especialLines = 0;
    unsigned int stackPos = 0;
    for(unsigned int i = 0; i < tokensToRun.size(); i++){
        switch(tokensToRun[i]){
            case nxPtr:
                stackPos++;
                break;
            case prevPtr:
                if(stackPos > 0) stackPos--;
                if(stackPos == 0) stackPos = 0;
                break;
            case on:
                printf("%d\n", stack[stackPos]);
                break;
            case oc:
                printf("%c", stack[stackPos]);
                break;
            case plus:
                stack[stackPos]++;
                break;
            case minus:
                stack[stackPos]--;
                break;
            case nxn:
                if(especialLines == 0){
                    stack[stackPos] = stringToUint16(finalString[i+1]);
                    especialLines += 2;
                }
                else{
                    stack[stackPos] = stringToUint16(finalString[i+especialLines]);
                    especialLines++;
                }
                break;
            case compj:
                if(stack[stackPos] == stack[stackPos+1]){
                    if(especialLines == 0){                    
                        i = stringToUint16(finalString[i+1]);
                        especialLines += 2;
                    }
                    else{
                        i = stringToUint16(finalString[i+especialLines]);
                        especialLines++;
                    }
                }
                break;
            case in:
                std::cin >> stack[stackPos];
                break;
            case noOp:
                printf("No operation\n");
                break;
            default:
            ;
        }
    }
    
    // for(unsigned int i = 0; i < finalString.size(); i++) printf("%s", finalString[i].c_str());
    // printf("\n");
    // for(unsigned int i = 0; i < finalString.size(); i++) printf("%d", tokensToRun[i]);
    // printf("\n");
    // printf("%d\n", stringToUint16("aashsdASHS"));
    return 0;
}

uint16_t stringToUint16(std::string s){
    uint16_t value = 0;
    char c[s.length()];
    strcpy(c, s.c_str());
    reverseCharArray(c, 0, strlen(c)-1);
    for(unsigned int i = 0; i < strlen(c); i++){
        value += (int)(c[i] - 48)*((int)pow(10, i));
    }
    // std::cout << value << "\n";
    return value;
}

void reverseCharArray(char* c, unsigned int start, unsigned int end){
    char temp;
    while(start < end){
        temp = c[start];
        c[start] = c[end];
        c[end] = temp;
        start++;
        end--;
    }
}