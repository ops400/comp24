# comp24A extremely simple interpreted programing language made in C++.
## De commands
### >
Goes to the next stack position.
### <
Goes to the previous stack position.
### on 
"Output Number", output the number in current stack position as a number.
### oc
"Output Character" output the number in current stack position as a character.
### +
Adds one to the number in the current stack position.
### -
Subtracts one to the number in the current stack position.
### nxn
"Next is Number", set the current stack position to value to the number in the next line.
### compj
"Compare and Jump" if the number in the current position in the stack and the next number in the stack are the same then set (or "jump") i in the main program for loop to number in the next line.
### in
"Input Number" set the current number in the stack as the user input.
### noOp
"No Operation" just does nothing.
## Its structure
There is a kinda non fixed size stack, that will store all the integer values in your program, comp24 is interpreted line by line. To move the position that you are in the stack use > and <, + and - to add and subtract by one, compj to jump around the code, on and oc to output as number and as character, in to get user input, and nxn to easily set the current value in the stack.
## "FAQ"
### Why?
Because it's fun!
### Are there any practical applications to use it?
No. Well someone could write something actually fun on it, like an text adventure rpg.
## To do(s)
- fix the especial lines bug (**side note:** I was thinking that doing a little different implementation of comp24 was a better idea).
