# COMP24
A imaginary 16 bit CPU architecture with a 24 bit address.
## The Architecture
It's pretty simple
- Little endian
- 4 registers and one adder
- A 24 bit memory address (MA0 ... MA23)
- It can do 16 bit operations
- 16 programable output & input (PIO0 ... PIO15)
- 8 pins for reading and writing to memory (MIO0 ... MIO7)
- A memory access output, high if it is accessing memory and low if not
- And a reset input
- The capacity to see if a subtraction gives a negative result
- 4 CPU states (C,Z,N,H)
- 24 bit program counter (PC)
- A "jump" pin, when high jump to address $000000
### The Registers and Adder
As mentioned before it has 4 register and one adder, they are all 16 bit and the adder will only store the result of last mathematical operation.<br>
The registers are W, X, Y and Z, you can store & load values from the adder or memory to them and do mathematical operations on them. You might think that the adder (or as I recently have come to call it the A register) is useless, but you could use it as a temporary location for storing a value and every time a branch operation is called its condition is checked on the A register. So lets say you did ```sbr w x``` and the result would be 0, that would be stored to A, and the next instruction would be ```jpz $70000F```, the branch if zero instruction would check the A register. The PIO also has its own register that the pins state is base on, so this register is called the PIO register(creative I know) or as I like to call it PIOR.
#### The Registers Numbering
```
A:    0
W:    1
X:    2
Y:    3
Z:    4
PIOR: 5
```
### The CPU States
- ```C```(Carry): 1 if it exceeds the 16 bit limit and 0 if not.<br>
- ```Z```(Zero): 1 when a instruction gives 0 and 0 if not.<br>
- ```N```(Negative): 1 when the result of subtraction is less than 0 or when the first register is smaller than the second on the ```cmp``` instruction, if none of the previous conditions are false then it's 0.<br>
- ```H```(Higher): 1 if the first register has a bigger value than the second on the ```cmp``` instruction, if not then is 0.
### The Peculiar First 5 Bytes
Well the 1st, 2nd, and 3rd are for the jump pin address, so like for the address ```567FDA```, the first five bytes would be ```0xDA 0x7F 0x56 ?? ??```. See I put question marks for the two last bytes, thats because those two last are to set which pins from the programmable I/O are for input and output. So it basically works by looking which bits are 0 and 1, 0 is for input and 1 is output, so lets say that the 4th byte is ```00``` and the 5th byte is ```FF```, that would set the first 5 bytes to ```0xDA 0x7F 0x56 0x00 0xFF```, as COMP24 is little endian that means the value that would configure which pins are for input and output would be ```FF00```, that value in binary would be ```1111111100000000``` the first bit is for PIO0 and the last for PIO15.<br>
For those who are wondering what all the PIOs would be, here they are.
```
PIO0 :1
PIO1 :1
PIO2 :1
PIO3 :1
PIO4 :1
PIO5 :1
PIO6 :1
PIO7 :1
PIO8 :0
PIO9 :0
PIO10:0
PIO11:0
PIO12:0
PIO13:0
PIO14:0
PIO15:0
```
### The Also Peculiar Last 2 Bytes
Those last two bytes are... quite badly executed, they are for seeing the state of a PIO that is set to input, and setting the state of a PIO that is set to output. So lets say that their configuration is ```FF00```, and the last two bytes are ```A901``` (remember as it stores byte per byte and it also is little endian, so in memory the last two bytes would be stored as ```0x01 0xA9```), as I and (I believe) many others have difficulties visualizing how this would look like so here it is a mediocre attempt.
```
PIO0 (1 output): 1 (set to high)
PIO1 (1 output): 0 (set to low)
PIO2 (1 output): 1 (set to high)
PIO3 (1 output): 0 (set to low)
PIO4 (1 output): 1 (set to high)
PIO5 (1 output): 0 (set to low)
PIO6 (1 output): 0 (set to low)
PIO7 (1 output): 1 (set to high)
PIO8 (0  input): 0 (no current being applied)
PIO9 (0  input): 0 (no current being applied)
PIO10(0  input): 0 (no current being applied)
PIO11(0  input): 0 (no current being applied)
PIO12(0  input): 0 (no current being applied)
PIO13(0  input): 0 (no current being applied)
PIO14(0  input): 0 (no current being applied)
PIO15(0  input): 1 (there is current being applied)
```
### Instructions Structure
The instructions are composed of 5 bytes, the first one is for the operation code, the second is for the register (A/W/X/Y/Z/PIOR), the third to fifth has three uses, the first use is for a memory address (occupying 24 bits), the second one is for storing a 16 bit value in the third and fourth byte and the last is for storing a register value (0: A, 1: W, 2: X, 3: Y, 4: Z, 5: PIOR) in the third byte.<br>
#### Exemples
##### Memory Address
1º byte: ```0x01``` ```LDM``` (Load from memory)<br>
2º byte: ```0x04``` ```Z``` (Z register)<br>
3º byte: ```0xA0```<br>
4º byte: ```0x0F```<br> 
5º byte: ```0x80```<br>
```0x800FA0``` (remember it's little endian) is the memory address that it will load, because ```LDM``` uses the third to fifth byte as the memory address.
##### Register Exemple
1º byte: ```0x02``` ```LDR``` (Load a Register)<br>
2º byte: ```0x02``` ```X``` (X register)<br>
3º byte: ```0x00``` ```A``` (A register)<br>
4º byte: ```0x00```<br> 
5º byte: ```0x00``` (It will not read the fifth byte nor the fourth byte) <br>
```0x00``` or the A register will be loaded to the X and the fourth and fifth byte will be ignored.
### Instructions
(**Side note:** I like listing the instructions in lower case, despite what I wrote before)<br>
```0x00```: ```nop``` (No Operation) Does nothing. **Exemple:** ```0x00 0x00 0x00 0x00 0x00```<br>
```0x01```: ```ldm``` (Load Memory) Loads a value from memory to a register. **Exemple:** ```0x01 0x02 0x00 0xAD 0x80``` (80AD00)<br>
```0x02```: ```ldr``` (Load Register) Copies the first register value to the second. **Exemple:** ```0x02 0x04 0x01 0x00 0x00```<br>
```0x03```: ```str``` (Store Register) Stores a registers value to RAM. **Exemple:** ```0x03 0x03 0x01 0xF4 0xA1``` (A1F401)<br>
```0x04```: ```stv``` (Store Value) Stores a value 8 bit to memory. **Exemple:** ```0x04 0xA1 0x67 0x6D 0xF0``` (F06D67)<br>
```0x05```: ```adm``` (Add Memory) Adds a value from RAM to a register and stores in A. **Exemple:** ```0x05 0x02 0xFF 0xFF 0xFF``` (FFFFFF)<br>
```0x06```: ```adr``` (Add Register) Adds two registers together and stores in A. **Exemple:** ```0x06 0x03 0x01 0x00 0x00```<br>
```0x07```: ```adv``` (Add Value) Adds a registers with a 16 bit value and stores in A. **Exemple:** ```0x07 0x05 0x01 0x0A 0x00``` (0A01)<br>
```0x08```: ```sbm``` (Subtract Memory) Subtracts the specified register by a value from RAM and stores in A. **Exemple:** ```0x08 0x01 0x01 0x00 0x80``` (800001)<br>
```0x09```: ```sbr``` (Subtract Register) Subtracts the first registers by the second and stores in A. **Exemple:** ```0x09 0x03 0x01 0x00 0x00```<br>
```0x0A```: ```sbv``` (Subtract Value) Subtracts a registers by the 16 bit value and stores in A. **Exemple:** ```0x0A 0x03 0x0F 0xA1 0x00``` (A10F)<br>
```0x0B```: ```jpm``` (Jump) Set PC to a 24 bit value. **Exemple:** ```0x0B 0x00 0xC3 0xF1 0x70``` (jumps to 70F1C3 in memory and ignores the second byte)<br>
```0x0C```: ```jpz``` (Jump Zero) Set PC to a 24 bit value if Z is 1<br>
```0x0D```: ```jpc``` (Jump Cary) Set PC to a 24 bit value if C is 1<br>
```0x0E```: ```jpn``` (Jump Negative) Set PC to a 24 bit value if N is 1<br>
```0x0F```: ```jph``` (Jump Higher) Set PC to a 24 bit value if H is 1<br>
```0x10```: ```cmp``` (Compare) Compares the first register by the second and stores the result relative to the first register to 3 CPU states (Z,N,H), Z if it's equal, N if it's less and H if it is higher. **Exemple:** ```0x10 0x01 0x04 0x00 0x00```<br>
```0x11```: ```brk``` (Break) COMP24 halts forever and can only go back to normal by doing a reset. **Exemple:** ```0x11 0x00 0x00 0x00 0x00```<br>
```0x12```: ```lsf``` (Left Shift) Shifts one bit to the left on a register. **Exemple:** ```0x12 0x05 0x00 0x00 0x00```<br>
```0x13```: ```rsf``` (Right Shift) Shifts one bit to the right on a register. **Exemple:** ```0x13 0x01 0x00 0x00 0x00```<br>
```0x14```: ```inr``` (Invert Register) Inverts a register like: 0110101101101011 to 1001010010010100; and stores it in the register that it inverted. **Exemple:** ```0x14 0x04 0x00 0x00 0x00```<br>
```0x15```: ```inc``` (Increment) Adds 1 to the referred register. **Exemple:** ```0x15 0x00 0x00 0x00 0x00```<br>
```0x16```: ```dec``` (Decrement) Subtracts 1 to the referred register. **Exemple:** ```0x16 0x03 0x00 0x00 0x00```<br>
```0x17```: ```mum``` (Multiply Memory) Multiplies a register with the value from the memory address specified and stores the result in A. **Example:** ```0x17 0x02 0xAF 0x01 0x86``` (8601AF)<br>
```0x18```: ```mur``` (Multiply Register) Multiplies a register with the value from the other register and stores the result in A. **Example:** ```0x18 0x04 0x01 0x00 0x00```<br>
```0x19```: ```muv``` (Multiply Value) Multiplies a register with the specified value and stores the result in A. **Example:** ```0x19 0x04 0xA4 0x8F 0x00``` (8FA4)<br>
```0x1A```: ```dim``` (Divide Memory) Divides the register by the value in the specified value in the memory address and stores in A. **Example:** ```0x1A 0x02 0xA4 0x8F 0xDE``` (DE8FA4)<br>
```0x1B```: ```dir``` (Divide Register) Divides the first register by the second register and stores the result in A. **Example:** ```0x1B 0x05 0x01 0x00 0x00```<br>
```0x1C```: ```div``` (Divide Value) Divides the first register by the specified value and stores the result in A. **Example:** ```0x1B 0x03 0x04 0x0F 0x00``` (0F04)<br>
**Exemple:** <br>
```0x08 0x02 0x03 0x00 0x00```<br>
```0x03 0x00 0x01 0x00 0x00```<br>
**Side note:** The N CPU state can represent two things. One of those things is when the COMP24 does a subtraction and the result is negative, but as it doesn't support signed numbers so it's like a carry bit, but for negative numbers. And the other purpose is to show when on compare instruction the first register is of a smaller value than the second.<br>
**Side note 2:** The ```dim```, ```dir``` and ```div``` does a integer division (the ```(int)(9/4)``` type, not the ```7%4``` type), like 9/4 = 2.
### The jump Pin
When high it executes the ```jpm``` instruction using the bytes in $000000, $000001 and $000002. So it would do something like:<br>```str #FF $000000```<br>```str #00 $000001```<br>```str #FF $000002```
### The reset pin
It sets the PC to $000003.

## The Simple COMP24 Assembly(SCA)
The SCA is the programming language that the "assembler" will read to make the binary that the emulator will run.
An exemple of the SCA syntax:
```
stv #03 $000000
stv #00 $000001
stv #00 $000002
ldm pior $80FF12
lsf pior
stv #12 $80FF13
stv #F0 $80FF14
ldm w #000F
brk
```

## To dos
Do the "assembler" (the thing that turns sca to machine code).

# In Memory of the previous COMP24 and of NGSPL (aka NGOSPEL)
